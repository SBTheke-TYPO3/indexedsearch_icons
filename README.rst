=============
Documentation
=============

----------------
What does it do?
----------------

- Nicer icons for search results of extension indexed_search.
- Support for more file types like .odt and .odp.
- Different icon sets (windows style in different icon sizes, icons from freepik).
- File width and height of icons adjustable.


-----------
Screenshots
-----------

.. figure:: Documentation/screenshot1.png

    Windows style, size 32x32.


.. figure:: Documentation/screenshot2.png

    Freepik icons, size 48x48 (please read readme.txt in icon folder for license details).


-------------
Configuration
-------------

- Install in extension manager
- Add the static template “Icons for indexed_search” in field “Include static (from extensions)” in your ROOT page
- Configure it with TypoScript constants


TS Constants
============

Your configuration options:

.. figure:: Documentation/screenshot3.png

    Configuration in constant editor.


----------
To-Do list
----------

You have ideas? Contact me!


---------
ChangeLog
---------

See file **ChangeLog** in the extension directory.